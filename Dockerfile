# Base image
FROM nginx:alpine

# Copy the custom nginx.conf file to the container
COPY nginx.conf /etc/nginx/nginx.conf

# Copy the built Vue.js application to the nginx root directory
COPY . /usr/share/nginx/html

# Expose port
EXPOSE 80

# Start the nginx server
CMD ["nginx", "-g", "daemon off;"]